Forked from [react-ultimate-resume](https://github.com/welovedevs/react-ultimate-resume).\
This resume is available as [Gitlab Page](https://bigoulours.gitlab.io/resume/react/).
![screenshot](screenshot.png)
## Installation
Clone this repository to the same folder where your resume.json file is located (otherwise add it as a submodule of your project):
```
git clone git@gitlab.com:bigoulours/react-resume.git
```
Change to the cloned folder and install dependencies:
```
cd react-resume
yarn install
```
Replace symlinks in `public` with your own favicon and images folder (banner is located at `images/banner.jpg` per default)

In `package.json`, change `homepage` to the path where you will be serving the page from (you can remove it altogether if you plan to serve the root directory).

Build:
```
yarn build
```
The `build` folder is now ready to be served on your server.