import { createContext } from 'react';

export const DeveloperProfileContext = createContext({});
export const StoreContext = createContext({});
type StaticDataContextType = {
    apiKeys: { giphy: string };
    endpoints: {
        devIcons: string;
    };
};
export const StaticDataContext = createContext<Partial<StaticDataContextType>>({});
