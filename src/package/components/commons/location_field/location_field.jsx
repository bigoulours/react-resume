import React, { useCallback, useEffect, useState } from 'react';

import cn from 'classnames';
import { createUseStyles } from 'react-jss';

import { TextField } from '@welovedevs/ui';

import { styles } from './location_field_style';

const useStyles = createUseStyles(styles);

const StubLocationField = ({ variant, onLocationSelected, value, clearOnSelect, onChange, fullWidth }) => {
    const [input, setInput] = useState(value);

    useEffect(() => {
        setInput(value);
    }, [value]);

    const handleChange = useCallback(
        (e) => {
            setInput(e.target.value);
            if (typeof onChange === 'function') {
                e.persist();
                onChange(e);
            }
        },
        [onChange]
    );
    const onKeyDown = useCallback(
        (e) => {
            if (e.keyCode === 13) {
                onLocationSelected({ name: input });
                if (clearOnSelect) {
                    setInput('');
                }
            }
        },
        [onLocationSelected, input, clearOnSelect]
    );
    return (
        <TextField
            variant={variant || 'outlined'}
            value={input}
            onChange={handleChange}
            onKeyDown={onKeyDown}
            clearOnSelect={clearOnSelect}
            fullWidth={fullWidth}
        />
    );
};

export const LocationField = ({
    variant,
    onLocationSelected,
    value,
    clearOnSelect,
    onChange,
    fullWidth,
    classes: receivedClasses = {}
}) => {
    const classes = useStyles();

    return (
        <div className={cn(classes.container, receivedClasses.container)}>
            <StubLocationField
                variant={variant}
                onLocationSelected={onLocationSelected}
                value={value}
                clearOnSelect={clearOnSelect}
                onChange={onChange}
                fullWidth={fullWidth}
                classes={classes}
            />
        </div>
    );
};
