import uuid from 'uuid/v4';

export const mapSocialFromJsonResume = (jsonResume) => ({
    socialNetworks: jsonResume?.basics?.profiles.map((profile, index) => ({
        ...profile,
        // generating uuid for manipulating data if not present
        id: profile.id || uuid(),
        index
    }))
});