import React, { useContext, memo, useMemo } from 'react';

import { useTheme } from 'react-jss';
import { getHexFromPaletteColor } from '../../utils/styles/styles_utils';
import { mapSocialFromJsonResume } from './data/mapping';
import { DeveloperProfileContext } from '../../utils/context/contexts';

const FooterComponent = () => {
    const theme = useTheme();
    const mainColor = getHexFromPaletteColor(theme, 'primary');

    const { data } = useContext(DeveloperProfileContext);
    const mappedData = useMemo(() => mapSocialFromJsonResume(data), [data]);
    const flexStyle = {
        alignItems: 'center', 
        justifyContent: 'center',
        "text-align": "center",
        padding: "1em"
    };

    return (
        <div className={'w-full mt-20 flex flex-col justify-center md:flex-row text-white'} style={{backgroundColor: mainColor}}>
            <div className={'flex flex-col text-lg text-white mt-4'}
                    style={flexStyle}>
                <p className={'mb-2 text-primary-50'}>Social media</p>
                <div className={'flex-1 flex flex-col mb-2.5'}>
                    <div className={'flex text-primary-50'}>
                        <>
                            {showSocialwLink(mappedData.socialNetworks)}
                        </>
                    </div>
                </div>
            </div>
        </div>
    );
};

function showSocialwLink(dat) {
    return(
        dat.map(profile => (
        <div style={{margin: '0.5em'}}>
            <a href={profile.url}>
                <i class={"fab fa-" + profile.network.toLowerCase() + " fa-2x"}></i>
            </a>
        </div>
    )
    )
    )
};

export const Footer = memo(FooterComponent);
