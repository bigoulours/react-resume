import { FormattedMessage } from 'react-intl';
import { Button } from '@welovedevs/ui';
import { useForm, ValidationError } from '@formspree/react';
import Popup from "reactjs-popup";
import { useTheme } from 'react-jss';

function CustomModalFormspree() {
  const theme = useTheme();
  const mainColor = theme.palette.primary[500];
  const secondColor = theme.palette.secondary[500];
  const secondColorLight = theme.palette.secondary[50];
  const contentStyle = {
    width: "26em",
    height: "32em",
    backgroundColor: "white",
    padding: "10px 10px 10px 10px",
    "border-radius": "5px",
    border: "solid 1px " + mainColor
    };
  
  const innerDivStyle = {
    overflow: "auto",
    width: "100%",
    height: "100%",
    padding: "10px",
  };
  
  const flexStyle = {
    alignItems: 'center', 
    justifyContent: 'center',
    "text-align": "center",
    padding: "1em",
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
};
  
  const form_style = {
    "flex-direction": "column",
  };

  const emailfield_style = {
    background: secondColorLight,
    width: "95%",
    "border-radius": "2px"
};
  
  const textarea_style = {
    background: secondColorLight,
    width: "95%",
    "border-radius": "2px",
    minHeight: "17em"
  };
  
  const cancel_style = {
    background: secondColor,
    color: "white",
    "border-radius": "5px",
    padding: "0.2em",
    margin: "1em",
    float: "left"
};

  const submit_style = {
    background: mainColor,
    color: "white",
    "border-radius": "5px",
    padding: "0.2em",
    margin: "1em",
    float: "right"
  };

  const close_style = {
    background: mainColor,
    color: "white",
    "border-radius": "5px",
    padding: "0.2em",
    margin: "1em"
};

  const [state, handleSubmit] = useForm(process.env.REACT_APP_FORMSPREE);
  if (state.succeeded) {
      return (
        <Popup
          trigger={<Button color={'light'} variant={'outlined'}>
                    <FormattedMessage id="Banner.sendMessageButton" defaultMessage="Contact Me" />
                  </Button>}
          modal
          contentStyle={contentStyle}
        >
          {close => (
          <div style={flexStyle}>
            Thanks for getting in touch!
            <br/>
            <button style={close_style} onClick={() => {close()}}>Close</button>
          </div>
          )
          }
        </Popup>
      )
  }
  return (
    <Popup
      trigger={<Button color={'light'} variant={'outlined'}>
                <FormattedMessage id="Banner.sendMessageButton" defaultMessage="Contact Me" />
              </Button>}
      modal
      contentStyle={contentStyle}
    >
      {close => (
    <div style={innerDivStyle}>
        <h3>Send me message</h3>
          <br/>
            <form onSubmit={handleSubmit} style={form_style}>
                <input
                id="email"
                type="email" 
                name="email"
                placeholder="Your email"
                style={emailfield_style}
                required
                />
                <ValidationError 
                prefix="Email" 
                field="email"
                errors={state.errors}
                />
                <br/>
                <br/>
                <textarea
                id="message"
                name="message"
                placeholder="Type a message"
                style={textarea_style}
                required
                />
                <ValidationError 
                prefix="Message" 
                field="message"
                errors={state.errors}
                />
                <br/>
                <br/>
                <button style={cancel_style} onClick={() => {close()}}>Cancel</button>
                <button type="submit" disabled={state.submitting} style={submit_style}>Submit</button>
            </form>
    </div>
      )
    }
    </Popup>
  );
}

const CustomModal = (process.env.REACT_APP_FORMSPREE ? CustomModalFormspree : "NoForm")

export default CustomModal;