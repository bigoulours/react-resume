import { useContext, useMemo } from 'react';

import { StaticDataContext } from '../../utils/context/contexts';

export const useCustomization = () => {
    const { customization } = useContext(StaticDataContext);
    const memoizedValue = useMemo(() => customization, [customization]);
    return [memoizedValue];
};
