import React, { useCallback, useContext, useMemo } from 'react';

import { FormattedMessage, useIntl } from 'react-intl';
import { createUseStyles } from 'react-jss';

import { useFormikContext } from 'formik';
import { Typography } from '@welovedevs/ui';

import { styles } from './experience_dialog_styles';
import { EditDialog } from '../../../../commons/edit_dialog/edit_dialog';
import { WorkValidator } from '../data/validator';

const useStyles = createUseStyles(styles);

const DEFAULT_OBJECT = {};
const ExperienceDialogComponent = ({ open, onClose, data: work, isEditing }) => {
    const classes = useStyles();

    const { formatMessage } = useIntl();

    const validator = useMemo(() => WorkValidator(formatMessage), []);

    return (
        <EditDialog
            classes={{ content: classes.container, paper: classes.paper }}
            open={open}
            onClose={onClose}
            data={work || DEFAULT_OBJECT}
            validationSchema={validator}
            isEditing={isEditing}
            title={<FormattedMessage id="Experience.editDialog.title" defaultMessage="Experience details" />}
        >
            {() => <ExperienceDialogContent isEditing={isEditing} />}
        </EditDialog>
    );
};

const ExperienceDialogContent = ({ isEditing }) => {
    const classes = useStyles();

    const { values: work } = useFormikContext();
    return (
        <>
            <Typography classes={{ container: classes.typography }} dangerouslySetInnerHTML={{__html: work.highlights.join('<br/>')}}/>
        </>
    );
};

export const ExperienceDialog = ExperienceDialogComponent;
