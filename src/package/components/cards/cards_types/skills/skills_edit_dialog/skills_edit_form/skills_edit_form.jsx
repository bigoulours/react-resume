import React, { useCallback } from 'react';

import { useTheme } from 'react-jss';
import { useFormikContext } from 'formik';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { Typography } from '@welovedevs/ui';
import uuid from 'uuid/v4';

import { TechnologiesPicker } from '../../../../../commons/technologies/technologies_picker';
import { useTechnologies } from '../../../../../hooks/technologies/use_technologies';

const SkillsEditFormComponent = ({ helpers: { handleValueChange } }) => {
    const theme = useTheme();
    const isMobile = useMediaQuery(`(max-width: ${theme.screenSizes.small}px)`);

    const { values, errors: validationErrors } = useFormikContext();

    const { technologies } = useTechnologies();

    const { skills: errors } = validationErrors;
    const addItem = useCallback(
        (name) =>
            handleValueChange(`skills[${values.skills.length}]`)({
                name,
                index: values.skills.length,
                value: 50,
                id: uuid()
            }),
        [values, handleValueChange]
    );
    const deleteItem = useCallback(
        (id) =>
            handleValueChange('skills')(
                values.skills.filter(({ id: skillId }) => skillId !== id).map((skill, index) => ({ ...skill, index }))
            ),
        [values.skills, handleValueChange]
    );
    const onArrayChange = useCallback((array) => handleValueChange('skills')(array), [handleValueChange]);
    const onItemChange = useCallback((item) => handleValueChange(`skills[${item.index}]`)(item), [handleValueChange]);

    return (
        <>
            {errors && (
                <Typography color="danger" component="p">
                    {errors}
                </Typography>
            )}
            <TechnologiesPicker
                technologies={technologies}
                selectedValues={values.skills}
                onAddItem={addItem}
                onArrayChange={onArrayChange}
                onArrayItemChange={onItemChange}
                onDeleteItem={deleteItem}
                isMobile={isMobile}
            />
        </>
    );
};

export const SkillsEditForm = SkillsEditFormComponent;
