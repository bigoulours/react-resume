import React, { useMemo } from 'react';

import { createUseStyles, useTheme } from 'react-jss';
import { motion } from 'framer-motion';

import { Typography } from '@welovedevs/ui/typography/typography';

import { getColorsFromCardVariant } from '../../../../../../utils/styles/styles_utils';

import { useCardVariant } from '../../../../../hooks/profile_card_hooks/use_card_variant';

import { styles } from './skills_keywords_styles';
import { DEFAULT_SPRING_TYPE as spring } from '../../../../../../utils/framer_motion/common_types/spring_type';

const useStyles = createUseStyles(styles);

const SkillsKeywords = ({ motionProps, skill }) => {
    const [variant] = useCardVariant();
    const classes = useStyles({ variant });
    const theme = useTheme();

    const color = useMemo(() => getColorsFromCardVariant(theme, variant).color, [theme, variant]);

    return (
        <motion.div
            className={classes.skillsKeywordsContainer}
            {...motionProps}
            transition={spring}
            style={{alignItems: 'center', 
            justifyContent: 'center',
            "text-align": "center",
            margin: "1em"}}
        >
            <Typography variant="h3" component="h3" color={color} classes={{ container: classes.skillsKeywordsTitle }}>
                {skill.name}
            </Typography>
            {skill.keywords.map((keyword) => (
                <Typography variant="h4" component="h4" color={color} classes={{ container: classes.skillLabel }}>
                {keyword}
            </Typography>
            ))}
        </motion.div>
    );
};

export default SkillsKeywords;
