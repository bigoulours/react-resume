import React, { useCallback, useContext, useMemo } from 'react';

import { FormattedMessage } from 'react-intl';

import { Button } from '@welovedevs/ui';

import { Dialog, DialogContent, DialogActions } from '@material-ui/core';

import { DialogTitle } from '../../commons/dialog/dialog_title/dialog_title';
import { DeveloperProfileContext } from '../../../utils/context/contexts';

import { FileDropZone } from '../../commons/file_drop_zone/file_drop_zone';

import { URLFailoverField } from '../../commons/url_failover_field/url_failover_field';

const EditBannerImageDialogComponent = ({ open, onClose, onChange }) => {
    const { onFilesUpload } = useContext(DeveloperProfileContext);
    const needsFailoverField = useMemo(() => !onFilesUpload, [onFilesUpload]);

    const onImageSelected = useCallback(
        (payload) => {
            onChange(payload);
            onClose();
        },
        [onChange, onClose]
    );

    const onImageChanged = useCallback(
        (payload) => {
            onChange(payload);
        },
        [onChange]
    );

    const onDrop = useCallback(
        (files) =>
            onFilesUpload(files).then((url) => {
                onImageSelected({ url });
                return url;
            }),
        [onImageSelected]
    );
    const onClear = useCallback(() => onImageSelected(null), [onImageSelected, onFilesUpload]);

    return (
        <>
            <Dialog open={open} onClose={onClose}>
                <DialogTitle>
                    <FormattedMessage id="Banner.EditImageDialog.Title" defaultMessage="Pick an image" />
                </DialogTitle>
                <DialogContent>
                    <FileDropZone disabled={!onFilesUpload} onDrop={onDrop} />
                    {needsFailoverField && <URLFailoverField onChange={onImageChanged} />}
                </DialogContent>
                <DialogActions>
                    <Button size="small" color="danger" onClick={onClear}>
                        <FormattedMessage id="Main.lang.clear" defaultMessage="Clear" />
                    </Button>
                    <Button size="small" onClick={onClose}>
                        <FormattedMessage id="Main.lang.close" defaultMessage="Close" />
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export const EditBannerImageDialog = EditBannerImageDialogComponent;
