import JsonStub from './data/json_stub.json';

function PageTitle() {
    return (
        "Profile - " + JsonStub.basics.name + " - " + JsonStub.basics.location?.city
    );
}

export default PageTitle;